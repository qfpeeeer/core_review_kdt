package main

import (
	"log"
	"whatsapp_go_collector/src/internal/app"
)

func main() {
	err := app.Exec()
	if err != nil {
		log.Fatalf(err.Error())
	}
}
