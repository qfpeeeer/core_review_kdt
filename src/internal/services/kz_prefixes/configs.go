package kz_prefixes

import (
	"whatsapp_go_collector/src/pkg/redis"
	"whatsapp_go_collector/src/pkg/utils"
)

type Configs struct {
	KzNumbersKey string        `yaml:"kz_numbers_key"`
	Redis        redis.Configs `yaml:"redis"`
	Utils        utils.Configs `yaml:"utils"`
}
