package kz_prefixes

type Service interface {
	ReserveKZPrefix() (int, error)
	MilestoneUpdate(oldNumber, currentNumber string) error
	ReleaseKZPrefix(currentNumber string) error
}
