package kz_prefixes

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strconv"
	"whatsapp_go_collector/src/pkg/redis"
	"whatsapp_go_collector/src/pkg/utils"
)

type service struct {
	configs Configs
	redis   redis.Service
	utils   utils.Service
}

func (s service) ReleaseKZPrefix(currentNumber string) error {
	if currentNumber == "" {
		return fmt.Errorf("passed empty string to input")
	}
	mapBody, err := s.getDocByID()
	if err != nil {
		return err
	}
	if _, exists := mapBody[currentNumber]; !exists {
		return fmt.Errorf("current number do not exists in responded dict, something went wrong")
	}

	mapBody[currentNumber] = "0"
	body, err := json.Marshal(mapBody)
	if err != nil {
		return fmt.Errorf("could not marshal saving body: %v", err)
	}
	err = s.redis.Set(s.configs.KzNumbersKey, body)
	if err != nil {
		return fmt.Errorf("could not save data to doc %v: %v", s.configs.KzNumbersKey, err)
	}
	return nil
}

func (s service) ReserveKZPrefix() (int, error) {
	mapBody, err := s.getDocByID()
	if err != nil {
		return -1, fmt.Errorf("could not get document by id: %v", err)
	}
	for k, v := range mapBody {
		valueAsString := fmt.Sprintf("%v", v)
		if valueAsString == "0" {
			err = s.updateFiled(k, "1", mapBody)
			if err != nil {
				return -1, fmt.Errorf("could not update field: %v", err)
			}

			intVar, err := strconv.Atoi(k)
			if err != nil {
				return -1, fmt.Errorf("could not convert number to integer: %v", err)
			}
			return intVar, nil
		}
	}
	return -1, fmt.Errorf("could not find any avaiable number")
}

func (s service) MilestoneUpdate(oldNumber, currentNumber string) error {
	if currentNumber == "" {
		return fmt.Errorf("passed empty string to input")
	}
	mapBody, err := s.getDocByID()
	if err != nil {
		return err
	}
	if _, exists := mapBody[oldNumber]; !exists {
		return fmt.Errorf("current number do not exists in responded dict, something went wrong")
	}
	val := mapBody[oldNumber]
	delete(mapBody, oldNumber)
	mapBody[currentNumber] = val

	body, err := json.Marshal(mapBody)
	if err != nil {
		return fmt.Errorf("could not marshal saving body: %v", err)
	}
	err = s.redis.Set(s.configs.KzNumbersKey, body)
	if err != nil {
		return fmt.Errorf("could not save data to doc %v: %v", s.configs.KzNumbersKey, err)
	}
	return nil
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
		redis:   redis.NewService(configs.Redis),
		utils:   utils.NewService(configs.Utils),
	}
}

func (s service) getDocByID() (map[string]string, error) {
	log.Infof("getting document with %v", s.configs.KzNumbersKey)

	body, err := s.redis.Get(s.configs.KzNumbersKey)
	if err != nil {
		return nil, fmt.Errorf("could not get document with %v id: %v", s.configs.KzNumbersKey, err)
	}

	if len(body) == 0 {
		return nil, fmt.Errorf("get from redis empty value")
	}

	var jsonMap map[string]string
	err = json.Unmarshal([]byte(body), &jsonMap)
	if err != nil {
		return nil, fmt.Errorf("could not unmarshal returned value: %v", err)
	}
	return jsonMap, nil
}

func (s service) updateFiled(key string, value string, mapBody map[string]string) error {
	mapBody[key] = value

	body, err := json.Marshal(mapBody)
	if err != nil {
		return fmt.Errorf("could not marshal saving body: %v", err)
	}
	err = s.redis.Set(s.configs.KzNumbersKey, body)
	if err != nil {
		return fmt.Errorf("[updateFiled] %v", err)
	}
	return nil
}
