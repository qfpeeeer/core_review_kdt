package whatsapp_client_service

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"go.mau.fi/whatsmeow"
	"go.mau.fi/whatsmeow/types"
	"whatsapp_go_collector/src/internal/domain/models"
	"whatsapp_go_collector/src/internal/services/whatsapp_client_init"
	"whatsapp_go_collector/src/pkg/token"
)

type service struct {
	configs        Configs
	WhatsappClient whatsapp_client_init.Service
	client         *whatsmeow.Client
	clientToken    token.Token
}

func (s *service) Init() error {
	return s.initWhatsappClient()
}

func (s *service) Stop() error {
	if s.client != nil {
		s.client.Disconnect()
	}
	return nil
}

func (s *service) GetWhatsappProfile(phoneNumber string) (models.WhatsappModel, error) {
	if s.client == nil {
		log.Infof("client is nil, trying to init new client...")
		err := s.initWhatsappClient()
		if err != nil {
			return models.WhatsappModel{}, fmt.Errorf("could not init whatsapp client: %v", err)
		}
	}

	if !s.client.IsConnected() {
		log.Infof("client is not connected, trying to make new connection...")
		err := s.changeWhatsappClient()
		if err != nil {
			return models.WhatsappModel{}, fmt.Errorf("could not change whatsapp client: %v", err)
		}
	}

	phones := make([]string, 1)
	phones[0] = "+" + phoneNumber
	isOnWhatsapp, err := s.client.IsOnWhatsApp([]string{phones[0]})
	if err != nil {
		return models.WhatsappModel{}, fmt.Errorf("could not send request to isOnWhatsapp: %v", err)
	}
	if !isOnWhatsapp[0].IsIn {
		return models.WhatsappModel{}, fmt.Errorf("number is not on whatsapp")
	}
	jids := make([]types.JID, 1)
	jids[0] = isOnWhatsapp[0].JID

	userInfo, _ := s.client.GetUserInfo(jids)
	userPhotoInfo, _ := s.client.GetProfilePictureInfo(jids[0], false, "")

	var photoModel models.Photo
	var profileModel models.WhatsappModel
	photoIsNull, bioIsNull := true, true
	if userPhotoInfo != nil {
		photoModel.Url = userPhotoInfo.URL
		profileModel.Photo = photoModel
		photoIsNull = false
	}
	profileModel.PlatformId = "whatsapp"
	profileModel.Id = isOnWhatsapp[0].JID.String()
	profileModel.Phone = isOnWhatsapp[0].JID.User
	profileModel.Url = "https://wa.me/" + phoneNumber
	if userInfo[jids[0]].Status != "" {
		profileModel.Bio = userInfo[jids[0]].Status
		bioIsNull = false
	}

	if photoIsNull && bioIsNull {
		return models.WhatsappModel{}, fmt.Errorf("profile do not contain useful information")
	}

	return profileModel, nil
}

func NewService(configs Configs) Service {
	return &service{
		configs:        configs,
		WhatsappClient: whatsapp_client_init.NewService(configs.WhatsappClient),
		client:         nil,
	}
}

func (s *service) initWhatsappClient() error {
	client, clientToken, err := s.WhatsappClient.GetWhatsappClient()
	if err != nil {
		return fmt.Errorf("could not init whatsapp client: %v", err)
	}
	s.client = client
	s.clientToken = clientToken
	return nil
}

func (s *service) changeWhatsappClient() error {
	client, clientToken, err := s.WhatsappClient.ChangeCurrentWhatsappClient(s.client, s.clientToken)
	if err != nil {
		return fmt.Errorf("could not change whatsapp client: %v", err)
	}
	s.client = client
	s.clientToken = clientToken
	return nil
}
