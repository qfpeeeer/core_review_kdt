package whatsapp_client_service

import (
	"whatsapp_go_collector/src/internal/services/whatsapp_client_init"
)

type Configs struct {
	WhatsappClient whatsapp_client_init.Configs `yaml:"whatsapp_init_client"`
}
