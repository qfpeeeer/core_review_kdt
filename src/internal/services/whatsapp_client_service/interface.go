package whatsapp_client_service

import (
	"whatsapp_go_collector/src/internal/domain/models"
)

type Service interface {
	Init() error
	GetWhatsappProfile(phoneNumber string) (models.WhatsappModel, error)
	Stop() error
}
