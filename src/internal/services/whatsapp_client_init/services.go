package whatsapp_client_init

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"go.mau.fi/whatsmeow"
	"go.mau.fi/whatsmeow/store/sqlstore"
	"net/http"
	"net/url"
	"whatsapp_go_collector/src/pkg/proxy"
	"whatsapp_go_collector/src/pkg/token"
)

type service struct {
	configs Configs
	token   token.Service
	proxy   proxy.Service
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
		proxy:   proxy.NewService(configs.Proxy),
		token:   token.NewService(configs.Token),
	}
}

func (s service) ChangeCurrentWhatsappClient(client *whatsmeow.Client, currentToken token.Token) (*whatsmeow.Client, token.Token, error) {
	if client == nil {
		return nil, token.Token{}, fmt.Errorf("whatsapp client do not passed")
	}

	log.Infof("chaning the client, botId - %v", currentToken.Id)

	err := s.deleteSession(client.Store.ID.User)
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not delete session: %v", err)
	}

	err = s.token.BanToken(currentToken)
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not ban old bot %v: %v", currentToken.Id, err)
	}

	newModel, err := s.token.GetToken()
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not get bot from token service: %v", err)
	}

	newClient, err := s.getWhatsappClientWithToken(newModel)
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not change bot, try again with new bot, old one is already banned: %v", err)
	}

	return newClient, newModel, nil
}

func (s service) GetWhatsappClient() (*whatsmeow.Client, token.Token, error) {
	newModel, err := s.token.GetToken()
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not get bot from token service: %v", err)
	}

	client, err := s.getWhatsappClientWithToken(newModel)
	if err != nil {
		return nil, token.Token{}, fmt.Errorf("could not get any bot: %v", err)
	}

	return client, newModel, nil
}

const (
	insertDeviceQuery = `
		INSERT INTO whatsmeow_device (jid, registration_id, noise_key, identity_key,
									  signed_pre_key, signed_pre_key_id, signed_pre_key_sig,
									  adv_key, adv_details, adv_account_sig, adv_device_sig,
									  platform, business_name, push_name)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
		ON CONFLICT (jid) DO UPDATE SET platform=$12, business_name=$13, push_name=$14
	`
	deleteDeviceQuery = `DELETE FROM whatsmeow_device WHERE jid=$1`
)

func (s service) saveSession(token token.Token) error {
	db, err := sql.Open("sqlite3", "file:session.db?_foreign_keys=on")
	log.Infof("Trying to save session for botId - %v", token.Id)
	if err != nil {
		return fmt.Errorf("could not open sql file: %v", err)
	}
	_, err = db.Exec(insertDeviceQuery,
		token.DeviceId, token.RegistrationID, token.NoiseKey, token.IdentityKey,
		token.SignedPreKey, token.SignedPreKeyKeyID, token.SignedPreKeySignature,
		token.AdvSecretKey, token.AccountDetails, token.AccountAccountSignature, token.AccountDeviceSignature,
		token.Platform, token.BusinessName, token.PushName)
	if err != nil {
		return fmt.Errorf("could not execute sql code: %v", err)
	}
	err = db.Close()
	if err != nil {
		return fmt.Errorf("could not close db: %v", err)
	}
	return err
}

func (s service) deleteSession(jid string) error {
	db, err := sql.Open("sqlite3", "file:session.db?_foreign_keys=on")
	if err != nil {
		return fmt.Errorf("could not open sql file: %v", err)
	}
	_, err = db.Exec(deleteDeviceQuery, jid)
	if err != nil {
		return fmt.Errorf("could not execute sql code: %v", err)
	}
	err = db.Close()
	if err != nil {
		return fmt.Errorf("could not close db: %v", err)
	}
	return err
}

func (s service) setProxyAddress(client *whatsmeow.Client) error {
	if client == nil {
		return fmt.Errorf("whatsapp client do not exist, please firstly try to run GetWhatsappClient function")
	}
	addr, err := s.proxy.GetProxy()
	if err != nil {
		return fmt.Errorf("could not get proxy address: %v", err)
	}
	parsed, err := url.Parse(addr)
	if err != nil {
		return err
	}
	client.SetProxy(http.ProxyURL(parsed))
	return nil
}

func (s service) pingWhatsappClient(client *whatsmeow.Client) error {
	if client == nil {
		return fmt.Errorf("whatsapp client do not exist, please firstly try to run GetWhatsappClient function")
	}
	health := client.IsConnected()
	if health == false {
		return fmt.Errorf("whatsapp client is not online")
	}
	return nil
}

func (s service) getWhatsappClientWithToken(model token.Token) (*whatsmeow.Client, error) {
	container, err := sqlstore.New("sqlite3", "file:session.db?_foreign_keys=on", nil)
	if err != nil {
		return nil, fmt.Errorf("whatsmeow container do not created: %v", err)
	}

	err = s.saveSession(model)
	if err != nil {
		return nil, fmt.Errorf("token save session error: %v", err)
	}

	deviceStore, err := container.GetFirstDevice()
	if err != nil {
		return nil, fmt.Errorf("device store error: %v", err)
	}

	client := whatsmeow.NewClient(deviceStore, nil)
	err = client.Connect()

	if s.pingWhatsappClient(client) != nil {
		err = s.token.BanToken(model)
		if err != nil {
			return nil, fmt.Errorf("could not ban bot %v: %v", model.Id, err)
		}
		return nil, fmt.Errorf("could not create new client from bot: %v", model.Id)
	}

	//err = s.setProxyAddress(client)
	//if err != nil {
	//	return nil, fmt.Errorf("could not set proxy for new client: %v", err)
	//}

	log.Infof("Successfully created client with %v bot", model.Id)
	return client, nil
}
