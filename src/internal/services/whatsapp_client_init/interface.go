package whatsapp_client_init

import (
	"go.mau.fi/whatsmeow"
	"whatsapp_go_collector/src/pkg/token"
)

type Service interface {
	GetWhatsappClient() (*whatsmeow.Client, token.Token, error)
	ChangeCurrentWhatsappClient(client *whatsmeow.Client, currentToken token.Token) (*whatsmeow.Client, token.Token, error)
}
