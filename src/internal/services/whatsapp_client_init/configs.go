package whatsapp_client_init

import (
	"whatsapp_go_collector/src/pkg/proxy"
	"whatsapp_go_collector/src/pkg/token"
)

type Configs struct {
	Token token.Configs `yaml:"token"`
	Proxy proxy.Configs `yaml:"proxy"`
}
