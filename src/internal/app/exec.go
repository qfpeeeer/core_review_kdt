package app

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
	"time"
	"whatsapp_go_collector/src/internal/usecase"
	"whatsapp_go_collector/src/pkg/configs"
)

func Exec() error {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat:   "15:04:05 02.01.2006",
		DisableHTMLEscape: true,
	})
	log.SetOutput(os.Stdout)

	// Загрузка конфигов
	log.Infof("loading configs..")
	var useCaseConfigs usecase.Configs
	err := configs.Init(&useCaseConfigs, "src/configs/configs.yaml")
	if err != nil {
		return fmt.Errorf("cannot load configs: %v", err)
	}

	// Создание сервиса useCase
	useCase := usecase.NewService(useCaseConfigs)
	err = useCase.Init()
	if err != nil {
		log.Errorf("failed to init app: %v", err)
	}

	// Основной сервис
	log.Infof("Start working..")
	go func() {
		err = useCase.Exec()
		if err != nil {
			log.Errorf("ERROR: %v", err)
		}
	}()

	//Перехват завершения приложения для закрытия всех процессов
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-done

	log.Infof("stopping services..")
	errors := useCase.Stop()
	for _, err = range errors {
		if err != nil {
			log.Infof("failed to stop service: %v", err)
		}
	}

	_, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	return nil
}
