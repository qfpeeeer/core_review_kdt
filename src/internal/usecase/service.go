package usecase

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"whatsapp_go_collector/src/internal/services/kz_prefixes"
	"whatsapp_go_collector/src/internal/services/whatsapp_client_service"
	"whatsapp_go_collector/src/pkg/elastic"
)

type service struct {
	configs         Configs
	KZNumbersPrefix kz_prefixes.Service
	WhatsappClient  whatsapp_client_service.Service
	elastic         elastic.Service
	ready           bool
	workingNumber   int
}

func (s *service) Exec() error {
	if s.ready == false || s.workingNumber == -1 {
		return fmt.Errorf("app is not ready to exec, use Init() func")
	}
	count := 0
	currentNumber := s.workingNumber
	maxNumber := s.getMaximumPossibleNumber(s.workingNumber)
	for currentNumber <= maxNumber {
		count += 1
		if count%300 == 0 {
			log.Infof("[Milestone achived!] Current number: %v", currentNumber)
			strOld := strconv.Itoa(s.workingNumber)
			strCurrent := strconv.Itoa(currentNumber)
			err := s.KZNumbersPrefix.MilestoneUpdate(strOld, strCurrent)
			s.workingNumber = currentNumber
			if err != nil {
				err = s.exitSaving(count, currentNumber)
				if err != nil {
					log.Warningf("could not do exit saving: %v", err)
				}
				return fmt.Errorf("could not update current number %v to %v at milestone: %v", strOld, strCurrent, err)
			}
		}

		str := strconv.Itoa(currentNumber)
		profile, err := s.WhatsappClient.GetWhatsappProfile(str)

		if err != nil && strings.Compare("number is not on whatsapp", err.Error()) == 0 {
			currentNumber += 1
			continue
		} else if err != nil && strings.Compare("profile do not contain useful information", err.Error()) == 0 {
			currentNumber += 1
			continue
		} else if err != nil {
			log.Warningf("could not get profile for number %v: %v", str, err)
			log.Infof("starting to exit saving...")
			err = s.exitSaving(count, currentNumber)
			if err != nil {
				log.Warningf("could not do exit saving: %v", err)
			}
			return fmt.Errorf("occured error while getting information of %v: %v", str, err)
		}

		err = s.elastic.Save(s.configs.Index, profile, true)
		if err != nil {
			err = s.exitSaving(count, currentNumber)
			if err != nil {
				log.Warningf("could not do exit saving: %v", err)
			}
			return fmt.Errorf("could not save whatsapp profile: %v", err)
		}
		currentNumber += 1
	}
	return nil
}

func (s *service) Stop() []error {
	errs := make([]error, 0)
	err := s.WhatsappClient.Stop()
	if err != nil {
		errs = append(errs, fmt.Errorf("could not stop whatsapp client: %v", err))
	}

	err = s.exitSaving(1, s.workingNumber)
	if err != nil {
		errs = append(errs, fmt.Errorf("could not release working number: %v", err))
	}

	s.elastic.Stop()
	return errs
}

func (s *service) exitSaving(count, currentNumber int) error {
	if count == 0 {
		log.Infof("[Exit Saving] Nothing to save!")
		return nil
	}

	log.Infof("[Exit Saving] Saving is started...")
	strOld := strconv.Itoa(s.workingNumber)
	strCurrent := strconv.Itoa(currentNumber)
	err := s.KZNumbersPrefix.MilestoneUpdate(strOld, strCurrent)
	if err != nil {
		return fmt.Errorf("could not update current number %v to %v at milestone: %v", strOld, strCurrent, err)
	}
	err = s.KZNumbersPrefix.ReleaseKZPrefix(strCurrent)
	if err != nil {
		return fmt.Errorf("could not release working number: %v", err)
	}
	return nil
}

func (s *service) getMaximumPossibleNumber(number int) int {
	number /= 10000000
	number *= 10000000
	number += 9999999
	return number
}

func (s *service) Init() error {
	log.Infof("Started init service...")

	log.Infof("Getting whatsapp client...")
	err := s.WhatsappClient.Init()
	if err != nil {
		return fmt.Errorf("could not get client, please add new bots and try again: %v", err)
	}

	log.Infof("Getting working numbers...")
	number, err := s.KZNumbersPrefix.ReserveKZPrefix()
	if err != nil {
		return fmt.Errorf("could not get number for work with: %v", err)
	}

	log.Infof("Working number is: %v", number)
	s.workingNumber = number
	s.ready = true
	return nil
}

func NewService(configs Configs) Service {
	return &service{
		configs:         configs,
		KZNumbersPrefix: kz_prefixes.NewService(configs.KZNumbersPrefix),
		WhatsappClient:  whatsapp_client_service.NewService(configs.WhatsappClient),
		elastic:         elastic.NewService(configs.Elastic),
		ready:           false,
		workingNumber:   -1,
	}
}
