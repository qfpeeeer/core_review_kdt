package usecase

import (
	"whatsapp_go_collector/src/internal/services/kz_prefixes"
	"whatsapp_go_collector/src/internal/services/whatsapp_client_service"
	"whatsapp_go_collector/src/pkg/elastic"
)

type Configs struct {
	Index           string                          `yaml:"whatsapp_collector_index"`
	KZNumbersPrefix kz_prefixes.Configs             `yaml:"kz_numbers_prefix"`
	WhatsappClient  whatsapp_client_service.Configs `yaml:"whatsapp_client"`
	Elastic         elastic.Configs                 `yaml:"elastic"`
}
