package usecase

type Service interface {
	Exec() error
	Stop() []error
	Init() error
}
