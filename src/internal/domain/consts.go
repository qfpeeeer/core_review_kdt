package domain

// RFC850
const (
	RFC850  = "Monday, 02-Jan-06 15:04:05 MST"
	ISO8601 = "2006-01-02 15:04:05"
	ISO8602 = "2006-01-02T15:04:05"
)
