package models

type FailedMessage struct {
	Message interface{} `json:"message"`
	Error   string      `json:"error,omitempty"`
}
