package models

import (
	"fmt"
	"github.com/mitchellh/hashstructure/v2"
)

type Photo struct {
	Url     string `json:"photo_url,omitempty"`
	Content string `json:"photo_content,omitempty"`
}

type WhatsappModel struct {
	PlatformId string   `json:"platform_id,omitempty"`
	ProfileId  string   `json:"profile_id,omitempty"`
	Id         string   `json:"id,omitempty"`
	Phone      string   `json:"phone,omitempty"`
	BirthDate  string   `json:"birth_date,omitempty"`
	Screenname string   `json:"screenname,omitempty"`
	Fullname   string   `json:"fullname,omitempty"`
	Url        string   `json:"url,omitempty"`
	Bio        string   `json:"bio,omitempty"`
	Photo      Photo    `json:"photo,omitempty"`
	Location   string   `json:"location,omitempty"`
	LastSeen   string   `json:"last_seen,omitempty"`
	Stories    []string `json:"stories,omitempty" hash:"set"`
	CreatedAt  string   `json:"created_at,omitempty" hash:"ignore"`
	UpdatedAt  string   `json:"updated_at,omitempty" hash:"ignore"`
}

func (m *WhatsappModel) GetHash() (string, error) {
	hash, err := hashstructure.Hash(m, hashstructure.FormatV2, nil)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d", hash), nil
}
