package utils

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/hashstructure/v2"
)

type service struct {
	configs Configs
}

func (s service) MapStringInterfaceToInterface(object map[string]interface{}) ([]interface{}, error) {
	var result []interface{}

	objectByteArray, err := json.Marshal(object)
	if err != nil {
		return nil, fmt.Errorf("[MapStringInterfaceToInterface] %v", err)
	}

	err = json.Unmarshal(objectByteArray, &result)
	if err != nil {
		return nil, fmt.Errorf("[MapStringInterfaceToInterface] %v", err)
	}

	return result, nil
}

func (s service) InterfaceToMapInterface(object interface{}) (map[string]interface{}, error) {
	var result map[string]interface{}

	objectByteArray, err := json.Marshal(object)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(objectByteArray, &result)
	if err != nil {
		return nil, err
	}

	return result, nil

}

func (s service) GetHash(m interface{}) (string, error) {
	hash, err := hashstructure.Hash(m, hashstructure.FormatV2, nil)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d", hash), nil
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
	}
}
