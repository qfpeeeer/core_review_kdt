package utils

type Service interface {
	GetHash(m interface{}) (string, error)
	InterfaceToMapInterface(object interface{}) (map[string]interface{}, error)
	MapStringInterfaceToInterface(object map[string]interface{}) ([]interface{}, error)
}
