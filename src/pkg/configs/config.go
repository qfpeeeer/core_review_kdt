package configs

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
)

func Init(configs interface{}, configsPath string) error {
	if reflect.TypeOf(configs).Kind() != reflect.Ptr {
		return fmt.Errorf("configs object must be ptr type")
	}

	data, err := ioutil.ReadFile(configsPath)
	if err != nil {
		return fmt.Errorf("read data err: %v", err)
	}

	dataStr := os.ExpandEnv(string(data))

	d := yaml.NewDecoder(strings.NewReader(dataStr))
	if err = d.Decode(configs); err != nil {
		return fmt.Errorf("decode error: %v", err)
	}

	emptyProperties := GetEmptyProperties(configs, "Config")
	if len(emptyProperties) > 0 {
		for _, property := range emptyProperties {
			fmt.Printf("empty property: %s\n", property)
		}
		return fmt.Errorf("config struct have empty properties")
	}

	return nil
}

func GetEmptyProperties(object interface{}, structName string) []string {
	properties := make([]string, 0)
	value := reflect.ValueOf(object)

	value, isStruct := getStructValue(value)
	if !isStruct {
		return nil
	}

	if value.NumField() == 0 {
		properties = append(properties, structName)
		return properties
	}

	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)
		fieldName := value.Type().Field(i).Name
		tag := value.Type().Field(i).Tag

		field, isStruct = getStructValue(field)

		if isStruct {
			if field.NumField() == 0 {
				continue
			}
			if key := tag.Get("config"); key == "omitzero" && field.IsZero() {
				continue
			}

			indirectProperties := GetEmptyProperties(field.Interface(), fieldName)
			for _, property := range indirectProperties {
				properties = append(properties, fmt.Sprintf("%s -> %s", structName, property))
			}
			continue
		}

		if key := tag.Get("config"); key == "omitempty" {
			continue
		}
		if valueIsEmpty(value.Field(i)) {
			properties = append(properties, fmt.Sprintf("%s -> %s", structName, fieldName))
		}
	}
	return properties
}

func getStructValue(value reflect.Value) (reflect.Value, bool) {
	if value.Kind() == reflect.Ptr {
		value = reflect.Indirect(value)
	}

	if value.Kind() != reflect.Struct {
		return reflect.ValueOf(nil), false
	}

	return value, true
}

func valueIsEmpty(value reflect.Value) bool {
	if value.Kind() == reflect.Ptr && value.IsNil() {
		return true
	} else if value.Kind() == reflect.Ptr {
		value = reflect.Indirect(value)
	}

	switch value.Kind() {
	case reflect.String:
		return value.String() == ""
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return value.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return value.Int() == 0
	case reflect.Float32, reflect.Float64:
		return value.Int() == 0
	case reflect.Array, reflect.Map, reflect.Slice:
		return value.IsNil()
	}

	return false
}
