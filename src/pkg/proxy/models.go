package proxy

import "time"

type Proxy struct {
	Login     string      `json:"login"`
	Password  string      `json:"password"`
	Addr      string      `json:"addr"`
	Port      int         `json:"port"`
	Protocol  string      `json:"protocol"`
	ExpiresAt time.Time   `json:"expires_at"`
	Country   string      `json:"country"`
	City      string      `json:"city"`
	IsActive  bool        `json:"is_active"`
	CreatedAt time.Time   `json:"created_at"`
	UpdatedAt time.Time   `json:"updated_at"`
	DeletedAt interface{} `json:"deleted_at"`
}
