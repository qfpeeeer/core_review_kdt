package proxy

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
)

type service struct {
	configs Configs
}

func (s service) GetProxy() (string, error) {
	var proxyList []Proxy
	client := &http.Client{}
	url := s.configs.URL
	r, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", fmt.Errorf("could not send request to %v: %v", url, err)
	}
	q := r.URL.Query()
	q.Add("package", s.configs.Package)
	q.Add("count", "1")

	r.URL.RawQuery = q.Encode()
	response, err := client.Do(r)
	if err != nil {
		return "", fmt.Errorf("could not send request to %v: %v", url, err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Infof("Error while closing body: %v", err)
		}
	}(response.Body)

	body, _ := ioutil.ReadAll(response.Body)
	err = json.Unmarshal(body, &proxyList)
	if err != nil {
		return "", fmt.Errorf("failed to get bot from avatar service: %v", err)
	}

	proxy := proxyList[0]

	log.Infof("Using proxy addr: %v", proxy.Addr)
	auth := fmt.Sprintf("%v:%v@", proxy.Login, proxy.Password)
	return fmt.Sprintf("%v://%v%v:%v", proxy.Protocol, auth, proxy.Addr, proxy.Port), nil
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
	}
}
