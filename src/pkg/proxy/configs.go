package proxy

type Configs struct {
	URL     string `yaml:"url"`
	Package string `yaml:"package"`
}
