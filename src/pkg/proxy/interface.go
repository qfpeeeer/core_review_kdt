package proxy

type Service interface {
	GetProxy() (string, error)
}
