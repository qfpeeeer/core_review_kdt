package token

type Service interface {
	GetToken() (Token, error)
	BanToken(Token) error
}
