package token

type Configs struct {
	TokenServiceUrl        string `yaml:"token_service_url"`
	TokenServiceDepartment string `yaml:"token_service_department"`
	TokenServiceEnv        string `yaml:"token_service_env"`
	TokenServiceResource   string `yaml:"token_service_resource"`
}
