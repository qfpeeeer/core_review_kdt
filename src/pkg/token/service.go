package token

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
)

type service struct {
	configs Configs
}

func (s service) GetToken() (Token, error) {
	var token Token
	client := &http.Client{}
	url := s.configs.TokenServiceUrl + "/reserve"
	log.Infof("Sending request to %v", url)
	r, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return Token{}, fmt.Errorf("could not send request to %v: %v", url, err)
	}
	q := r.URL.Query()
	q.Add("department", s.configs.TokenServiceDepartment)
	q.Add("resource", s.configs.TokenServiceResource)
	q.Add("env", s.configs.TokenServiceEnv)
	q.Add("count", "1")
	q.Add("is_valid", "true")

	r.URL.RawQuery = q.Encode()
	response, err := client.Do(r)
	if err != nil {
		return Token{}, fmt.Errorf("could not send request to %v: %v", url, err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Infof("Error while closing body: %v", err)
		}
	}(response.Body)

	//body, _ := ioutil.ReadAll(response.Body)
	//body := [123 34 97 99 99 111 117 110 116 95 97 99 99 111 117 110 116 95 115 105 103 110 97 116 117 114 101 34 58 34 80 55 57 104 65 114 114 69 82 79 82 105 71 83 80 117 89 110 78 73 56 72 56 56 100 66 57 85 49 114 98 80 56 47 80 120 43 112 119 53 70 108 122 71 72 108 73 47 99 97 66 120 118 119 114 115 79 110 104 74 112 112 67 106 85 100 108 55 117 100 80 88 75 102 99 53 102 119 49 88 85 109 48 122 68 65 61 61 34 44 34 97 99 99 111 117 110 116 95 100 101 116 97 105 108 115 34 58 34 67 80 121 57 122 102 65 72 69 75 109 83 104 90 73 71 71 65 99 61 34 44 34 97 99 99 111 117 110 116 95 100 101 118 105 99 101 95 115 105 103 110 97 116 117 114 101 34 58 34 57 115 67 86 107 71 117 113 106 75 66 48 66 117 122 71 120 118 97 121 118 109 117 49 69 56 118 111 103 48 119 73 116 84 100 120 110 70 97 74 51 82 114 104 65 72 52 109 113 110 71 112 88 116 74 86 74 109 108 119 115 121 83 56 56 106 77 90 118 121 119 79 78 106 87 101 73 47 51 66 72 48 77 54 67 103 61 61 34 44 34 97 100 118 95 115 101 99 114 101 116 95 107 101 121 34 58 34 84 79 78 114 57 77 48 57 99 73 109 52 79 84 120 110 122 121 55 56 89 100 56 55 48 89 117 99 80 43 56 49 88 76 71 101 50 82 120 102 86 100 85 61 34 44 34 98 117 115 105 110 101 115 115 95 110 97 109 101 34 58 34 34 44 34 99 114 101 97 116 101 100 95 97 116 34 58 34 50 48 50 50 45 48 51 45 50 56 84 48 53 58 51 53 58 52 48 90 34 44 34 99 114 101 97 116 101 100 95 98 121 34 58 34 114 46 115 104 97 108 107 101 110 98 97 121 101 118 64 107 97 122 100 114 101 97 109 46 107 122 34 44 34 100 101 112 97 114 116 109 101 110 116 34 58 34 119 101 98 105 110 116 34 44 34 100 101 118 105 99 101 95 105 100 34 58 34 52 57 49 55 54 51 48 54 53 56 54 51 57 46 48 58 54 55 64 115 46 119 104 97 116 115 97 112 112 46 110 101 116 34 44 34 101 110 118 34 58 34 112 114 111 100 34 44 34 105 100 34 58 34 54 50 52 49 52 57 50 99 54 99 100 100 54 97 101 53 50 98 99 52 102 56 50 99 34 44 34 105 100 101 110 116 105 116 121 95 107 101 121 34 58 34 81 75 72 106 116 102 76 75 72 86 52 53 50 104 67 76 66 78 55 122 88 77 81 49 115 66 121 56 78 107 100 70 109 87 114 108 85 43 65 47 48 72 119 61 34 44 34 110 111 105 115 101 95 107 101 121 34 58 34 79 75 101 85 70 57 88 55 104 108 65 122 48 110 53 49 78 48 85 87 116 114 56 57 67 72 109 117 74 102 122 56 81 115 82 47 113 56 56 49 84 86 115 61 34 44 34 112 108 97 116 102 111 114 109 34 58 34 97 110 100 114 111 105 100 34 44 34 112 117 115 104 95 110 97 109 101 34 58 34 34 44 34 114 101 103 105 115 116 114 97 116 105 111 110 95 105 100 34 58 50 53 57 54 57 57 54 49 54 50 44 34 114 101 115 111 117 114 99 101 34 58 34 119 104 97 116 115 97 112 112 34 44 34 115 104 97 114 101 100 34 58 102 97 108 115 101 44 34 115 105 103 110 101 100 95 112 114 101 95 107 101 121 34 58 34 54 79 78 109 54 120 65 114 89 51 111 118 77 65 75 101 112 65 48 70 53 52 43 120 111 117 120 110 80 113 121 107 104 122 110 121 109 97 122 107 89 72 48 61 34 44 34 115 105 103 110 101 100 95 112 114 101 95 107 101 121 95 107 101 121 95 105 100 34 58 49 44 34 115 105 103 110 101 100 95 112 114 101 95 107 101 121 95 115 105 103 110 97 116 117 114 101 34 58 34 116 51 100 110 50 55 75 80 79 90 114 110 83 90 99 85 97 110 121 53 110 65 68 77 114 83 104 85 103 117 90 90 104 57 118 78 52 43 80 119 116 54 83 55 113 66 86 102 70 82 90 72 97 73 105 98 69 48 51 67 77 111 121 117 98 49 104 105 86 103 74 50 75 115 50 88 104 115 73 55 104 108 99 74 67 65 61 61 34 44 34 115 116 97 116 117 115 34 58 49 44 34 117 112 100 97 116 101 100 95 97 116 34 58 34 50 48 50 50 45 48 54 45 49 51 84 48 54 58 51 52 58 52 56 46 57 51 49 90 34 44 34 117 115 97 98 108 101 95 97 116 34 58 34 50 48 50 50 45 48 51 45 50 56 84 48 53 58 51 53 58 52 48 90 34 125]
	body := []byte(`{"status":1,"department":"webint","env":"prod","resource":"whatsapp","shared":false,"created_at":"2022-12-01T19:12:23.211524941+06:00","updated_at":"2022-12-01T19:12:23.211525294+06:00","usable_at":"2022-12-01T19:12:23.211525464+06:00","created_by":"r.shalkenbayev@kazdream.kz","device_id":"77006773456.0:4@s.whatsapp.net","registration_id":2596996162,"noise_key":"wLqckEHPie0NbY10j2CZe6Kl1VHTfOWCEeSboL+pYlo=","identity_key":"IL3pWzH2XtSXlsAfz5UmoUB652BYtASQvj5rSurxXF8=","signed_pre_key":"EDt8/5FSe/6km0W0xnrRtz1RW1M3m7RiKMyYTa52Vls=","signed_pre_key_key_id":1,"signed_pre_key_signature":"QftGilSeaGswPQGoefF44a9xh/V59TljY/jLVBNZEjJPbgUJGTHdjwoS64Mjc4I2I9ZpOtXVtt0sN/DFVD//AQ==","adv_secret_key":"apz+MVOst+A6jvXfEbQS+jO1eWwDHntEe1FLYozAf3M=","account_details":"CIeKrsEBELbQopwGGAQ=","account_account_signature":"3jCqi6vEbeaAWJYm7thpHswnd+Q3DOTXI573G9hwx9gCmx/6UaZHXaf/YkRsMPxdXu/LQMBsVYkSzDf09VaGBw==","account_device_signature":"G1r2H3GNiDWTCcu+Ps31duni8nqqTPSgiSeMuxw5SWuMznQ4iJAYQILCIjlKLmio0GIxIE8FNl8UlyguRf+2CQ==","platform":"android","business_name":"","push_name":""}`)
	//fmt.Println(string(body))
	err = json.Unmarshal(body, &token)

	if err != nil && len(body) > 0 {
		return Token{}, fmt.Errorf("failed to get bot from avatar service: %v", err)
	}

	if string(token.NoiseKey) == "" {
		return Token{}, fmt.Errorf("failed to get bot from avatar service, returned empty value: %v", err)
	}
	log.Infof("Using bot with token id: %v", token.Id)

	return token, nil
}

func (s service) BanToken(token Token) error {
	client := &http.Client{}
	url := s.configs.TokenServiceUrl + "/tokens"
	jsonStr, err := json.Marshal(token)
	if err != nil {
		return fmt.Errorf("could not marshal token body, err: %v", err)
	}
	r, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return fmt.Errorf("could not send request to %v: %v", url, err)
	}
	r.Header.Set("Accept", "application/json")

	response, err := client.Do(r)
	if err != nil {
		return fmt.Errorf("could not send request to %v: %v", url, err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Infof("Error while closing body: %v", err)
		}
	}(response.Body)

	body, _ := ioutil.ReadAll(response.Body)
	log.Infof("response Status: %v", response.StatusCode)
	log.Infof("response Headers: %v", response.Header)
	log.Infof("response Body: %v", string(body))
	log.Infof("Banned bot with token id: %v", token.Id)
	return nil
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
	}
}
