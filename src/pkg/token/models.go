package token

import "time"

type Token struct {
	Id                      string    `json:"id"`
	Status                  int32     `json:"status,omitempty"`
	Department              string    `json:"department" json:"department,omitempty"`
	Env                     string    `json:"env" json:"env,omitempty"`
	Resource                string    `json:"resource"`
	Shared                  bool      `json:"shared"`
	CreatedAt               time.Time `json:"created_at"`
	UpdatedAt               time.Time `json:"updated_at"`
	UsableAt                time.Time `json:"usable_at"`
	CreatedBy               string    `json:"created_by"`
	DeviceId                string    `json:"device_id"`
	RegistrationID          uint32    `json:"registration_id"`
	NoiseKey                []byte    `json:"noise_key"`
	IdentityKey             []byte    `json:"identity_key"`
	SignedPreKey            []byte    `json:"signed_pre_key"`
	SignedPreKeyKeyID       uint32    `json:"signed_pre_key_key_id"`
	SignedPreKeySignature   []byte    `json:"signed_pre_key_signature"`
	AdvSecretKey            []byte    `json:"adv_secret_key"`
	AccountDetails          []byte    `json:"account_details"`
	AccountAccountSignature []byte    `json:"account_account_signature"`
	AccountDeviceSignature  []byte    `json:"account_device_signature"`
	Platform                string    `json:"platform"`
	BusinessName            string    `json:"business_name"`
	PushName                string    `json:"push_name"`
}
