package connection

import "github.com/go-redis/redis/v8"

type Service interface {
	GetClient() (*redis.Client, error)
}
