package connection

type Configs struct {
	RedisUrl      string `yaml:"redis_url"`
	RedisPort     string `yaml:"redis_port"`
	RedisPassword string `yaml:"redis_password"`
	RedisDB       string `yaml:"redis_db"`
}
