package connection

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"strconv"
)

type service struct {
	configs Configs
	client  *redis.Client
}

func (s service) getClient(count int) (*redis.Client, error) {
	if count == 0 {
		return nil, fmt.Errorf("too many retry connections to redis")
	}

	if s.client != nil {
		if err := s.client.Ping(context.TODO()).Err(); err != nil {
			log.Infof("failed to ping redis client, recreating client...: %v", err)
			s.client = nil
			log.Infof("starting to retry to redis, retry count: %v", count)
			return s.getClient(count - 1)
		}
		return s.client, nil
	}
	log.Info("[REDIS] connecting to Redis...")
	redisDB, err := strconv.Atoi(s.configs.RedisDB)
	if err != nil {
		return nil, fmt.Errorf("could not convert redisDB string to int %v: %v", s.configs.RedisDB, err)
	}
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", s.configs.RedisUrl, s.configs.RedisPort),
		//Password: s.configs.RedisPassword,
		DB: redisDB,
	})

	if err = redisClient.Ping(context.TODO()).Err(); err != nil {
		return nil, fmt.Errorf("failed to ping Redis client, error: %v", err)
	}
	log.Infof("successfully connnected to redis, address %v", s.configs.RedisUrl)
	return redisClient, nil
}

func (s service) GetClient() (*redis.Client, error) {
	if s.client != nil {
		if err := s.client.Ping(context.TODO()).Err(); err != nil {
			log.Infof("failed to ping redis client, recreating client...: %v", err)
			s.client = nil
			return s.getClient(5)
		}
		return s.client, nil
	}
	log.Info("[REDIS] connecting to Redis...")
	redisDB, err := strconv.Atoi(s.configs.RedisDB)
	if err != nil {
		return nil, fmt.Errorf("could not convert redisDB string to int %v: %v", s.configs.RedisDB, err)
	}
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", s.configs.RedisUrl, s.configs.RedisPort),
		//Password: s.configs.RedisPassword,
		DB: redisDB,
	})

	if err = redisClient.Ping(context.TODO()).Err(); err != nil {
		return nil, fmt.Errorf("failed to ping Redis client, error: %v", err)
	}
	log.Infof("successfully connnected to redis, address %v", s.configs.RedisUrl)
	return redisClient, nil
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
		client:  nil,
	}
}
