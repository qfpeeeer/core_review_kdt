package redis

import "whatsapp_go_collector/src/pkg/redis/connection"

type Configs struct {
	Redis connection.Configs `yaml:"redis_connection"`
}
