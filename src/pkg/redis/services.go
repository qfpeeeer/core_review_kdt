package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"time"
	"whatsapp_go_collector/src/pkg/redis/connection"
)

type service struct {
	configs    Configs
	connection connection.Service
}

func (s service) Get(key string) (string, error) {
	redisClient, err := s.connection.GetClient()
	if err != nil {
		return "", fmt.Errorf("could not init redis service: %v", err)
	}
	cachedValue, err := redisClient.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return "", fmt.Errorf("key does not exist: %v", err)
	} else if err != redis.Nil && err != nil {
		return "", fmt.Errorf("failed to get data in cache: key - %s, err - %v", key, err)
	}
	return cachedValue, nil
}

func (s service) Set(key string, value interface{}) error {
	redisClient, err := s.connection.GetClient()
	if err != nil {
		return fmt.Errorf("could not init redis service: %v", err)
	}

	if err := redisClient.Set(context.Background(), key, value, time.Hour*1).Err(); err != nil {
		log.Errorf("failed to save data in cache: key - %s, value - %v, err - %v", key, value, err)
		return err
	}
	return nil
}

func NewService(configs Configs) Service {
	return &service{
		configs:    configs,
		connection: connection.NewService(configs.Redis),
	}
}
