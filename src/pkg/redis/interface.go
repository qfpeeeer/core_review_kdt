package redis

type Service interface {
	Get(key string) (string, error)
	Set(key string, value interface{}) error
}
