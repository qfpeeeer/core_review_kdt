package elastic

import "github.com/olivere/elastic/v7"

type Service interface {
	GetClient() (*elastic.Client, error)
	Get(query elastic.Query, index string, size, from int) ([]interface{}, int, error)
	GetByDocID(docId string, index string) (interface{}, error)
	MultipleSave(index string, data []interface{}, addTime bool) error
	Save(index string, data interface{}, addTime bool) error
	SaveToDocID(index string, docID string, data []interface{}, addTime bool) error

	Stop()
}
