package elastic

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic/v7"
	log "github.com/sirupsen/logrus"
	"time"
	"whatsapp_go_collector/src/pkg/utils"
)

type service struct {
	configs Configs
	utils   utils.Service
	client  *elastic.Client
}

func (s *service) Stop() {
	s.client.Stop()
}

func NewService(configs Configs) Service {
	return &service{
		configs: configs,
		utils:   utils.NewService(configs.Utils),
	}
}

func (s *service) GetClient() (*elastic.Client, error) {
	if s.client != nil {
		return s.client, nil
	}
	client, err := elastic.NewSimpleClient(elastic.SetBasicAuth(s.configs.ElasticUser, s.configs.ElasticPassword),
		elastic.SetURL(s.configs.ElasticURL))
	if err != nil {
		return nil, fmt.Errorf("failed to connect elastic: %v", err)
	}

	log.Infof("successfully get elastic client")
	s.client = client
	return client, nil
}

func (s *service) Get(query elastic.Query, index string, size, from int) ([]interface{}, int, error) {
	log.Infof("[ELASTIC]: requesting to elastic")
	client, err := s.GetClient()
	if err != nil {
		return nil, 0, err
	}
	result, err := client.Search(index).Query(query).Size(size).From(from).Do(context.Background())
	if err != nil {
		return nil, 0, err
	}

	data := make([]interface{}, 0)
	for _, hit := range result.Hits.Hits {
		var obj interface{}
		err = json.Unmarshal(hit.Source, &obj)
		if err != nil {
			return nil, 0, err
		}
		data = append(data, obj)
	}
	return data, int(result.Hits.TotalHits.Value), nil
}

func (s *service) GetByDocID(docId string, index string) (interface{}, error) {
	log.Infof("[ELASTIC]: requesting to elastic")
	var obj interface{}
	client, err := s.GetClient()
	if err != nil {
		return nil, fmt.Errorf("[GetByDocId] could not get elastic client: %v", err)
	}
	result, err := client.Index().Index(index).Id(docId).BodyJson(&obj).Do(context.Background())
	if err != nil {
		return nil, fmt.Errorf("[GetByDocId] could not make requests to elastic client: %v", err)
	}

	err = json.Unmarshal([]byte(result.Result), &obj)
	if err != nil {
		return nil, fmt.Errorf("[GetByDocId] could not unmarshal responded body: %v", err)
	}
	return obj, nil

}

func (s *service) MultipleSave(index string, data []interface{}, addTime bool) error {
	log.Infof("[ELASTIC]: saving data")
	client, err := s.GetClient()
	if err != nil {
		return fmt.Errorf("failed to get client: %v", err)
	}
	ctx := context.Background()

	for _, object := range data {
		id, err := s.utils.GetHash(&object)
		if err != nil {
			return err
		}
		currentTime := time.Now().UTC().Format("2006-01-02T15:04:05")

		objectMap, err := s.utils.InterfaceToMapInterface(object)
		if err != nil {
			return err
		}

		if addTime {
			objectMap["created_at"] = currentTime
			objectMap["updated_at"] = currentTime
		}
		get, err := client.Get().Index(index).Id(id).Do(ctx)
		if err == nil {
			var found map[string]interface{}
			err = json.Unmarshal(get.Source, &found)
			if err != nil {
				return err
			}

			if addTime && found["created_at"] != "" {
				objectMap["created_at"] = found["created_at"]
			}
		}

		if _, err = client.Index().Index(index).Id(id).BodyJson(objectMap).Do(ctx); err != nil {
			return err
		}
		log.Infof("[ELASTIC]: creating/updating a document with id - %s is finished", id)
	}

	return nil

}

func (s *service) Save(index string, data interface{}, addTime bool) error {
	log.Infof("[ELASTIC]: saving data")
	client, err := s.GetClient()
	if err != nil {
		return fmt.Errorf("failed to get client: %v", err)
	}
	ctx := context.Background()

	id, err := s.utils.GetHash(&data)
	if err != nil {
		return err
	}
	currentTime := time.Now().UTC().Format("2006-01-02T15:04:05")

	objectMap, err := s.utils.InterfaceToMapInterface(data)
	if err != nil {
		return err
	}

	if addTime {
		objectMap["created_at"] = currentTime
		objectMap["updated_at"] = currentTime
	}
	get, err := client.Get().Index(index).Id(id).Do(ctx)
	if err == nil {
		var found map[string]interface{}
		err = json.Unmarshal(get.Source, &found)
		if err != nil {
			return err
		}

		if addTime && found["created_at"] != "" {
			objectMap["created_at"] = found["created_at"]
		}
	}

	if _, err = client.Index().Index(index).Id(id).BodyJson(objectMap).Do(ctx); err != nil {
		return err
	}
	log.Infof("[ELASTIC]: creating/updating a document with id - %s is finished", id)

	return nil

}

func (s *service) SaveToDocID(index string, docID string, data []interface{}, addTime bool) error {
	log.Infof("[ELASTIC]: saving data to doc with id: %v", docID)
	client, err := s.GetClient()
	if err != nil {
		return err
	}
	ctx := context.Background()

	for _, object := range data {
		currentTime := time.Now().UTC().Format("2006-01-02T15:04:05")

		objectMap, err := s.utils.InterfaceToMapInterface(object)
		if err != nil {
			return err
		}

		if addTime {
			objectMap["created_at"] = currentTime
			objectMap["updated_at"] = currentTime
		}

		get, err := client.Get().Index(index).Id(docID).Do(ctx)
		if err == nil {
			var found map[string]interface{}
			err = json.Unmarshal(get.Source, &found)
			if err != nil {
				return err
			}

			if addTime && found["created_at"] != "" {
				objectMap["created_at"] = found["created_at"]
			}
		}

		if _, err = client.Index().Index(index).Id(docID).BodyJson(objectMap).Do(ctx); err != nil {
			return err
		}
		log.Infof("[ELASTIC]: creating/updating a document with id - %s is finished", docID)
	}

	return nil
}
