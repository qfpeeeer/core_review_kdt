package elastic

import "whatsapp_go_collector/src/pkg/utils"

type Configs struct {
	ElasticUser     string        `yaml:"elastic_user"`
	ElasticPassword string        `yaml:"elastic_password"`
	ElasticURL      string        `yaml:"elastic_url"`
	Utils           utils.Configs `yaml:"utils"`
}
