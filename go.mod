module whatsapp_go_collector

go 1.18

require (
	github.com/go-redis/redis/v8 v8.11.5
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/mitchellh/hashstructure/v2 v2.0.2
	github.com/olivere/elastic/v7 v7.0.32
	github.com/sirupsen/logrus v1.9.0
	go.mau.fi/whatsmeow v0.0.0-20221122081206-059049466d44
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.mau.fi/libsignal v0.0.0-20221015105917-d970e7c3c9cf // indirect
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
